﻿using System.Threading.Tasks;
using Recruitment.Contracts;

namespace Recruitment.API.Services {
    public interface IHashCalculatorInvoker
    {
        Task<HashCalculationResult> CalcAsync(User user);
    }
}
