﻿using System.Threading.Tasks;
using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Recruitment.Contracts;

namespace Recruitment.API.Services.Imps {
    public class HashCalculatorInvoker : IHashCalculatorInvoker {
        private readonly IOptions<FunctionOptions> _options;
        public HashCalculatorInvoker(IOptions<FunctionOptions> options) {
            _options = options;
        }

        public async Task<HashCalculationResult> CalcAsync(User user) {
            FlurlHttp.Configure(settings => {
                var jsonSettings = new JsonSerializerSettings {
                    NullValueHandling = NullValueHandling.Ignore,
                    ObjectCreationHandling = ObjectCreationHandling.Replace
                };
                settings.JsonSerializer = new NewtonsoftJsonSerializer(jsonSettings);
            });
            return await _options.Value.Url.PostJsonAsync(user).ReceiveJson<HashCalculationResult>();
        }
    }
}
