﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Recruitment.API.Services;
using Recruitment.Contracts;

namespace Recruitment.API.Controllers {
    [ApiController]
    [Route("api")]
    public class HashController : ControllerBase {
        private readonly IHashCalculatorInvoker _calculatorInvoker;
        public HashController(IHashCalculatorInvoker calculatorInvoker) {
            _calculatorInvoker = calculatorInvoker;
        }

        [HttpPost("hash")]
        public async Task<ActionResult> Calculate([FromBody] User model) {
            return Ok(await _calculatorInvoker.CalcAsync(model));
        }
    }
}
