﻿using Newtonsoft.Json;

namespace Recruitment.Contracts {
    public class HashCalculationResult {
        [JsonProperty("hash_value")]
        public string HashValue { get; set; }
    }
}
