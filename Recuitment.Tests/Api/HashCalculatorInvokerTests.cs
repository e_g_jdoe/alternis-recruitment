﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Flurl.Http.Testing;
using Microsoft.Extensions.Options;
using Moq;
using Newtonsoft.Json;
using Recruitment.API;
using Recruitment.API.Services.Imps;
using Recruitment.Contracts;
using Xunit;

namespace Recuitment.Tests.Api {
    public class HashCalculatorInvokerTests {
        private readonly HashCalculatorInvoker _invoker;
        private readonly Mock<IOptions<FunctionOptions>> _options;
        public HashCalculatorInvokerTests() {
            _options = new Mock<IOptions<FunctionOptions>>();
            _invoker = new HashCalculatorInvoker(_options.Object);
        }

        [Fact]
        public async Task CalcAsync_Should_Posts() {
            _options.SetupGet(z => z.Value).Returns(new FunctionOptions { Url = "https://localhost" });
            using var httpTest = new HttpTest();
            await _invoker.CalcAsync(new User());
            httpTest.ShouldHaveCalled("*").WithVerb(HttpMethod.Post).Times(1);
        }

        [Fact]
        public async Task CalcAsync_Should_Posts_Url_Provided_In_Options() {
            const string expectedUrl = "http://this.is.sparta";
            _options.SetupGet(z => z.Value).Returns(new FunctionOptions { Url = expectedUrl });
            using var httpTest = new HttpTest();
            await _invoker.CalcAsync(new User());
            httpTest.ShouldHaveCalled(expectedUrl).WithVerb(HttpMethod.Post).Times(1);
        }

        [Fact]
        public async Task CalcAsync_Should_Posts_Users_Received_In_Args() {
            var testUser = new User { Login = "This is test login", Password = "This is test login" };
            _options.SetupGet(z => z.Value).Returns(new FunctionOptions { Url = "https://localhost" });
            using var httpTest = new HttpTest();
            await _invoker.CalcAsync(testUser);
            httpTest.ShouldHaveCalled("*").WithRequestBody(JsonConvert.SerializeObject(testUser)).Times(1);
        }

        [Fact]
        public async Task CalcAsync_Should_Returns_Whatever_Is_Returned_By_Http_Request() {
            var expectedResult = new HashCalculationResult { HashValue = "This is expected hash value" };
            _options.SetupGet(z => z.Value).Returns(new FunctionOptions { Url = "https://localhost" });
            using var httpTest = new HttpTest();
            httpTest.ForCallsTo("*").RespondWith(JsonConvert.SerializeObject(expectedResult));
            var result = await _invoker.CalcAsync(new User());
            Assert.Equal(expectedResult.HashValue, result.HashValue);
        }
    }
}
