﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Recruitment.API.Controllers;
using Recruitment.API.Services;
using Recruitment.Contracts;
using Xunit;

namespace Recuitment.Tests.Api {
    public class HashControllerTests {
        private readonly HashController _controller;
        private readonly Mock<IHashCalculatorInvoker> _invoker;
        public HashControllerTests() {
            _invoker = new Mock<IHashCalculatorInvoker>();
            _controller = new HashController(_invoker.Object);
        }

        [Fact]
        public void HashController_Should_Be_Api_Controller() {
            var hasAny = typeof(HashController).GetCustomAttributes(typeof(ApiControllerAttribute), true).Any();
            Assert.True(hasAny);
        }

        [Fact]
        public void Base_Route_For_HashController_Should_Be_Api() {
            var isApi = typeof(HashController).GetCustomAttributes(typeof(RouteAttribute), true)
                                              .OfType<RouteAttribute>()
                                              .Select(z => z.Template)
                                              .Any(z => string.Equals(z, "api", StringComparison.OrdinalIgnoreCase));
            Assert.True(isApi);
        }

        [Fact]
        public void Calculate_Should_Be_Of_Post_Method() {
            var isPots = typeof(HashController).GetMethod(nameof(HashController.Calculate))
                                               .GetCustomAttributes(typeof(HttpPostAttribute), true).Any();
            Assert.True(isPots);
        }

        [Fact]
        public void hash_Url_Should_Lead_To_Calculate_Method() {
            var isPots = typeof(HashController).GetMethod(nameof(HashController.Calculate))
                                               .GetCustomAttributes(typeof(HttpPostAttribute), true)
                                               .OfType<HttpPostAttribute>()
                                               .Any(z => string.Equals("hash", z.Template, StringComparison.OrdinalIgnoreCase));
            Assert.True(isPots);
        }

        [Fact]
        public async Task Calculate_Should_Invokes_HashCalculatorInvoker() {
            var _ = await _controller.Calculate(new User());
            _invoker.Verify(z => z.CalcAsync(It.IsAny<User>()), Times.Once);
        }

        [Fact]
        public async Task Calculate_Should_Invokes_HashCalculatorInvoker_With_User_Provided_In_Arg() {
            var user = new User { Password = "This is test pass", Login = "And this is test login" };
            var _ = await _controller.Calculate(user);
            _invoker.Verify(z => z.CalcAsync(user), Times.Once);
        }

        [Fact]
        public async Task Calculate_Should_Returns_Ok_Result() {
            var result = await _controller.Calculate(new User());
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Calculate_Should_Returns_HashCalculationResult() {
            _invoker.Setup(z => z.CalcAsync(It.IsAny<User>())).Returns(Task.FromResult(new HashCalculationResult()));
            var result = (OkObjectResult)await _controller.Calculate(new User());
            Assert.IsType<HashCalculationResult>(result.Value);
        }

        [Fact]
        public async Task Calculate_Should_Returns_Whatever_Is_Returned_From_Invoker() {
            var expectedResult = new HashCalculationResult { HashValue = "this is test hash" };
            _invoker.Setup(z => z.CalcAsync(It.IsAny<User>())).Returns(Task.FromResult(expectedResult));
            var result = (HashCalculationResult)((OkObjectResult)await _controller.Calculate(new User())).Value;
            Assert.Equal(expectedResult, result);
        }
    }
}
