﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Recruitment.Functions;
using Recruitment.Functions.Services;
using Moq;
using Newtonsoft.Json;
using Recruitment.Contracts;
using Xunit;

namespace Recuitment.Tests.Functions {
    public class HashCalculatorFunctionTests {
        private readonly HashCalculatorFunction _calculatorFunction;
        private readonly Mock<ICalculator> _calculator;
        private readonly ILogger _logger;
        public HashCalculatorFunctionTests() {
            _calculator = new Mock<ICalculator>();
            _logger = new Mock<ILogger>().Object;
            _calculatorFunction = new HashCalculatorFunction(_calculator.Object);
        }

        [Fact]
        public async Task Run_Should_Returns_BadRequest_If_Data_No_Valid_Json() {
            var data = "This is not valid json";
            var request = CreateHttpRequest(data);
            var result = await _calculatorFunction.Run(request, _logger);
            Assert.IsType<BadRequestResult>(result);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task Run_Should_Returns_BadRequest_If_Data_Is_Empty(string data) {
            var request = CreateHttpRequest(data);
            var result = await _calculatorFunction.Run(request, _logger);
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Run_Should_Returns_Bad_Request_If_Data_Is_Not_Valid_User() {
            var data = JsonConvert.SerializeObject(new { not_Valid = 1 });
            var request = CreateHttpRequest(data);
            var result = await _calculatorFunction.Run(request, _logger);
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async Task Run_Should_Not_Invokes_Calculator_If_Data_Is_Not_Valid_Json() {
            var data = "This is not valid json";
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(It.IsAny<string>()), Times.Never);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public async Task Run_Should_Not_Invokes_Calculator_If_Data_Is_Empty(string data) {
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task Run_Should_Not_Invokes_Calculator_If_Data_Is_Not_Valid_User() {
            var data = JsonConvert.SerializeObject(new { not_valid = 1 });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async Task Run_Should_Invokes_Calculator_If_Provided_Data_Is_Valid_User() {
            var password = Guid.NewGuid().ToString();
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new User { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Run_Should_Invokes_Calculator_If_Provided_Data_Is_Valid_Duck_Typed_User() {
            var password = Guid.NewGuid().ToString();
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task Run_Should_Invokes_Calculator_With_Provided_Login_And_Password() {
            var password = Guid.NewGuid().ToString();
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new User { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate($"{login}{password}"), Times.Once);
        }

        [Fact]
        public async Task Run_Should_Invokes_Calculator_Only_With_Password_If_Provided_Login_Is_Null() {
            var password = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new User { Password = password, Login = null });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(password), Times.Once);
        }

        [Fact]
        public async Task Run_Should_Invokes_Calculator_Only_With_Login_If_Provided_Password_Is_Null() {
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new User { Password = null, Login = login });
            var request = CreateHttpRequest(data);
            var _ = await _calculatorFunction.Run(request, _logger);
            _calculator.Verify(z => z.Calculate(login), Times.Once);
        }

        [Theory]
        [InlineData("TestLogin", "TestPassword")]
        [InlineData("TestLogin", null)]
        [InlineData(null, "TestPassword")]
        [InlineData(null, null)]
        public async Task Run_Should_Returns_Ok_Result(string login, string password) {
            var data = JsonConvert.SerializeObject(new User { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var result = await _calculatorFunction.Run(request, _logger);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async Task Run_Should_Returns_Ok_Result_For_Duck_Typed_User() {
            var password = Guid.NewGuid().ToString();
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var result = await _calculatorFunction.Run(request, _logger);
            Assert.IsType<OkObjectResult>(result);
        }
        [Theory]
        [InlineData("TestLogin", "TestPassword")]
        [InlineData("TestLogin", null)]
        [InlineData(null, "TestPassword")]
        [InlineData(null, null)]
        public async Task Run_Should_Returns_HashCalculationResult(string login, string password) {
            var data = JsonConvert.SerializeObject(new User { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var result = (OkObjectResult)await _calculatorFunction.Run(request, _logger);
            Assert.IsType<HashCalculationResult>(result.Value);
        }

        [Fact]
        public async Task Run_Should_Returns_HashCalculationResult_For_Duck_Typed_User() {
            var password = Guid.NewGuid().ToString();
            var login = Guid.NewGuid().ToString();
            var data = JsonConvert.SerializeObject(new { Password = password, Login = login });
            var request = CreateHttpRequest(data);
            var result = (OkObjectResult)await _calculatorFunction.Run(request, _logger);
            Assert.IsType<HashCalculationResult>(result.Value);
        }

        [Fact]
        public async Task Run_Should_Returns_Whatever_Is_Returned_From_Calculator_As_A_Hash_Value() {
            const string expectedResult = "This is expected result";
            var data = JsonConvert.SerializeObject(new User { Password = "Pass", Login = "Login" });
            var request = CreateHttpRequest(data);
            _calculator.Setup(z => z.Calculate(It.IsAny<string>())).Returns(expectedResult);
            var result = (HashCalculationResult)((OkObjectResult)await _calculatorFunction.Run(request, _logger)).Value;
            Assert.Equal(expectedResult, result.HashValue);
        }


        [Fact]
        public async Task Run_Should_Rethrows_Exception_Thrown_By_Calculator() {
            var expectedException = new Exception("This is test exception");
            var data = JsonConvert.SerializeObject(new User { Password = "Pass", Login = "Login" });
            var request = CreateHttpRequest(data);
            _calculator.Setup(z => z.Calculate(It.IsAny<string>())).Throws(expectedException);
            var ex = await Assert.ThrowsAsync<Exception>(() => _calculatorFunction.Run(request, _logger));
            Assert.Equal(expectedException, ex);
        }


        private static HttpRequest CreateHttpRequest(string data) {
            var context = new DefaultHttpContext();
            var stream = data == null ? new MemoryStream() : new MemoryStream(Encoding.UTF8.GetBytes(data));
            context.Request.Body = stream;
            context.Request.ContentLength = stream.Length;
            return context.Request;
        }

    }
}
