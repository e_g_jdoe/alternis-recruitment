﻿using Recruitment.Functions.Services.Imps;
using Xunit;

namespace Recuitment.Tests.Functions {
    public class Md5CalculatorTests {
        //Test Vector data: https://tools.ietf.org/html/rfc1321
        [Theory]
        [InlineData("", "d41d8cd98f00b204e9800998ecf8427e")]
        [InlineData("a", "0cc175b9c0f1b6a831c399e269772661")]
        [InlineData("abc", "900150983cd24fb0d6963f7d28e17f72")]
        [InlineData("message digest", "f96b697d7cb7938d525a2f31aaf161d0")]
        [InlineData("abcdefghijklmnopqrstuvwxyz", "c3fcd3d76192e4007dfb496cca67e13b")]
        [InlineData("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789", "d174ab98d277d9f5a5611c2c9f419d9f")]
        [InlineData("12345678901234567890123456789012345678901234567890123456789012345678901234567890", "57edf4a22be3c955ac49da2e2107b67a")]
        public void Should_Returns_MD5_Hash(string input, string expectedResult) {
            var cal = new Md5Calculator();
            var result = cal.Calculate(input);
            Assert.Equal(expectedResult, result);
        }

        [Fact]
        public void Should_Returns_Null_If_Input_Is_Null() {
            var cal = new Md5Calculator();
            var result = cal.Calculate(null);
            Assert.Null(result);
        }
    }
}
