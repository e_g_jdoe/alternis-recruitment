﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Recruitment.Functions.Services.Imps {
    public class Md5Calculator : ICalculator {
        public string Calculate(string data) {
            if (data is null) {
                return null;
            }

            using var hash = MD5.Create();
            return string.Join(
                "",
                from ba in hash.ComputeHash(Encoding.UTF8.GetBytes(data))
                select ba.ToString("x2")
            );
        }
    }
}
