﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Recruitment.Functions.Services {
    public interface ICalculator {
        string Calculate(string data);
    }
}
