using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Recruitment.Contracts;
using Recruitment.Functions.Services;

namespace Recruitment.Functions {
    public class HashCalculatorFunction {
        private readonly ICalculator _calculator;
        public HashCalculatorFunction(ICalculator calculator) {
            _calculator = calculator;
        }

        [FunctionName("HashCalculatorFunction")]
        public async Task<IActionResult> Run([HttpTrigger(AuthorizationLevel.Function, "post", Route = null)] HttpRequest req, ILogger log) {
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            if (!TryGetUser(requestBody, log, out var user)) {
                return new BadRequestResult();
            }
            var result = new HashCalculationResult {
                HashValue = _calculator.Calculate($"{user.Login ?? ""}{user.Password ?? ""}")
            };
            return new OkObjectResult(result);
        }

        public static bool TryGetUser(string data, ILogger log, out User result) {
            try {
                result = JsonConvert.DeserializeObject<User>(data, new JsonSerializerSettings { MissingMemberHandling = MissingMemberHandling.Error, NullValueHandling = NullValueHandling.Include});
                return result != null;
            } catch (Exception ex) {
                result = null;
                log.LogError(ex, "Something went wrong during deserialization of posted data");
                return false;
            }
        }
    }
}
